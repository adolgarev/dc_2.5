#ifndef WORKER_H_
#define WORKER_H_

#include <pthread.h>
#include <ev.h>

#include "util.h"
#include "request.h"

typedef enum _chan_err_e {
    chan_ok,
    chan_busy,
    chan_empty,
    chan_err
} chan_err_e;

int init_workers_io();
int start_worker();
chan_err_e send_request(request_s *request);
chan_err_e read_response(request_s **response);

#endif //WORKER_H_
