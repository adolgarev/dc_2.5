
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "worker.h"
#include "config.h"
#include "log.h"
#include "cmd.h"

typedef struct _worker_s {
    pthread_t worker_thread;
} worker_s;

static int req_pipe[2];
static int res_pipe[2];
pthread_mutex_t req_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t res_mutex = PTHREAD_MUTEX_INITIALIZER;

static void *worker_thread(void *arg);

//-------------------------------------------------------------------

int init_workers_io() {
    if (pipe(req_pipe) || pipe(res_pipe)) {
        log_syserr("cannot create pipe");
        goto error;
    }

    setnonblocking(req_pipe[1]);
    setnonblocking(res_pipe[0]);

    return res_pipe[0];
error:
    return 0;
}

//-------------------------------------------------------------------
int start_worker() {
    worker_s *worker = NULL;
    
    worker = malloc(sizeof(worker_s));
    if (!worker) {
        log_syserr("cannot allocate memory");
        goto error;
    }
    memset(worker, 0 , sizeof(worker_s));

    if (pthread_create(&worker->worker_thread, NULL, worker_thread, worker)) {
        log_syserr("cannot create thread");
        goto error;
    }
    
    return 0;
error:
    if (worker) {
        free(worker);
    }
    return 1;
}

//-------------------------------------------------------------------
chan_err_e send_request(request_s *request) {
    if (write(req_pipe[1], (void*)&request, sizeof(void*)) == -1) {
        if ((EAGAIN == errno || EWOULDBLOCK == errno)) {
            return chan_busy;
        }
        log_syserr("cannot write to pipe");
        return chan_err;
    }
    return chan_ok;
}

//-------------------------------------------------------------------
chan_err_e read_response(request_s **response) {

    if ( read(res_pipe[0], (void*)response, sizeof(void*)) == -1) {
        if ((EAGAIN == errno || EWOULDBLOCK == errno)) {
            return chan_empty;
        }
        log_syserr("cannot read from pipe");
        return chan_err;
    }
    return chan_ok;
}

//-------------------------------------------------------------------
static void *worker_thread(void *arg) {
    request_s *request = 0;
    int n;

    while(1) {

        pthread_mutex_lock( &req_mutex );
        n = read(req_pipe[0], (void*)&request, sizeof(void*));
        pthread_mutex_unlock( &req_mutex );

        if (n < 0) {
            log_syserr("read failed");
            goto error;
        }
        else if (n == 0)
            break;

        // Process request
        cmds[request->command](request);

        pthread_mutex_lock( &res_mutex );
        n = write(res_pipe[1], (void*)&request, sizeof(void*));
        pthread_mutex_unlock( &res_mutex );

        if ( n == -1) {
            log_syserr("cannot write to pipe");
            goto error;
        }
    }
error:
    pthread_exit(NULL);
}
//-------------------------------------------------------------------
