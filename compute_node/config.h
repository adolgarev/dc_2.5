#ifndef CONFIG_H_
#define CONFIG_H_


#define HOST            "127.0.0.1"
#define PORT            "4000"
#define BACKLOG         10
#define WORKERS_NUM     100

// Flow control
#define REQUEST_HIGH_WATERMARK  200000
#define REQUEST_LOW_WATERMARK   150000

// Memory manager
#define PREALLOC_REQ_NUM      80000
#define PREALLOC_DATA_NUM      5000


#define MPOINT          "/mnt"
#define MPOINTNUM       36

#endif //CONFIG_H_
