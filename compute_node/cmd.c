
#include <stdio.h>
#include <sys/types.h>
#include <attr/xattr.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>

#include "config.h"
#include "cmd.h"
#include "log.h"



static int cmd_head(request_s *request);
static int cmd_put(request_s *request);
static int cmd_get(request_s *request);

cmd cmds[] = {
    [command_head] = cmd_head,
    [command_put] = cmd_put,
    [command_get] = cmd_get
};

// /mnt + / + 04 + / + populating|live + / + ab / + key + '\0'
#define MAXFILENAME     (sizeof(MPOINT) + 1 + 2 + 1 + 10 + 1 + 2 + 1 + 40 + 1)
#define XATTR_GEN_NUMBER        "user.gennumber"

typedef enum file_loc_e_ {
    file_populating,
    file_live
} file_loc_e;

static char *dirs[] = {
    [file_populating] = "populating",
    [file_live] = "live"
};


//-------------------------------------------------------------------
static int lookup_name(unsigned char *key, file_loc_e file_loc, char *name) {
    uint64_t mpoint;
    size_t len = KEY_LEN * 2 + 1;
    char buf[len];
    
    {
        char    *p;
        
        for (p = buf; p < buf + len - 1; p++) {
            uint8_t r;
            r = key[(p-buf)/2];
            if ((p-buf) % 2)
                r &= 0x0F;
            else
                r >>= 4;
            if (r < 10)
                r += 48;
            else
                r += 87;
            *p = r;
        }
        buf[len - 1] = '\0';
    }
    
    memcpy(&mpoint, key, sizeof(mpoint));
    
    sprintf(name, "%s/%02X/%s/%s/%s", MPOINT, (unsigned int)(mpoint % MPOINTNUM), dirs[file_loc], buf + len - 3, buf);
    
    return 0;
}

//-------------------------------------------------------------------
int cmd_head(request_s *request) {
    char name[MAXFILENAME];
    struct stat statbuf;

    if (lookup_name(request->key, file_live, name))
        goto error;
    
    if (stat(name, &statbuf) == -1) {
        if (errno == ENOENT || errno == ENOTDIR)
            goto not_found;
        log_syserr("stat");
        goto error;
    }

    if (getxattr(name, XATTR_GEN_NUMBER, request->gen_number, GEN_NUM_LEN) == -1) {
        log_syserr("cannot get xattr for %s", name);
        goto error;
    }

    request->code = code_ok;
    return 0;
not_found:
    request->code = code_not_found;
    return 0;
error:
    request->code = code_error;
    return 1;
}

//-------------------------------------------------------------------
int cmd_put(request_s *request) {
    char live_name[MAXFILENAME];
    char populating_name[MAXFILENAME];
    struct stat statbuf;
    int fd = -1;
    
    if (lookup_name(request->key, file_live, live_name))
        goto error;
    
    if (stat(live_name, &statbuf) == -1) {
        if (errno == ENOENT || errno == ENOTDIR)
            goto not_found;
        log_syserr("stat");
        goto error;
    }
    
    request->code = code_ok;
    return 0;
    
not_found:
    if (lookup_name(request->key, file_populating, populating_name))
        goto error;
    
    log_debug("populating %s", populating_name);

    fd = open(populating_name, O_CREAT | O_TRUNC | O_WRONLY | O_SYNC, S_IWUSR | S_IRUSR | S_IWGRP | S_IRGRP | S_IROTH);
    if (fd == -1) {
        log_syserr("open");
        goto error;
    }
    
    {
        size_t n;
        
        for (n = 0; n < DATA_LEN; ) {
            int ret = write(fd, request->data + n, DATA_LEN - n);
            if (ret == -1) {
                log_syserr("write");
                goto error;
            }
            n += ret;
        }
    }
    
    if (fsetxattr(fd, XATTR_GEN_NUMBER, request->gen_number, GEN_NUM_LEN, XATTR_CREATE) == -1) {
        log_syserr("cannot create xattr for %s", populating_name);
        goto error;
    }
    
    close(fd);
    fd = -1;
    
    // atomic rename
    if (rename(populating_name, live_name) == -1) {
        log_syserr("rename");
        goto error;
    }
    
    log_debug("moving %s to %s", populating_name, live_name);

    request->code = code_ok;
    return 0;

error:
    if (fd != -1) {
        close(fd);
        unlink(populating_name);
    }
    request->code = code_error;
    return 1;
}

//-------------------------------------------------------------------
int cmd_get(request_s *request) {
    char name[MAXFILENAME];
    struct stat statbuf;
    int fd = -1;

    if (lookup_name(request->key, file_live, name))
        goto error;
    
    if (stat(name, &statbuf) == -1) {
        if (errno == ENOENT || errno == ENOTDIR)
            goto not_found;
        log_syserr("stat");
        goto error;
    }
    
    if ((fd = open(name, O_RDONLY)) == -1) {
        log_syserr("open");
        goto error;
    }
    
    {
        size_t n;
        
        for (n = 0; n < DATA_LEN; ) {
            int ret = read(fd, request->data + n, DATA_LEN - n);
            if (ret == -1) {
                log_syserr("write");
                goto error;
            }
            else if (ret == 0) {
                log_err("unexpected eof");
                goto error;
            }
            n += ret;
        }
    }

    if (fgetxattr(fd, XATTR_GEN_NUMBER, request->gen_number, GEN_NUM_LEN) == -1) {
        log_syserr("cannot get xattr for %s", name);
        goto error;
    }

    close(fd);
    fd = -1;

    request->code = code_ok;
    return 0;
    
not_found:
    request->code = code_not_found;
    return 0;
error:
    if (fd != -1)
        close(fd);
    request->code = code_error;
    return 1;
}
//-------------------------------------------------------------------
