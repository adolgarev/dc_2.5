#!/bin/sh

for i in `seq 0 35`; do

    DIR_NAME=`printf "%02X" $i`

    umount /mnt/$DIR_NAME
    mkfs.xfs -f -d agcount=10 -l lazy-count=1,size=128m /dev/etherd/e0.$i

    mkdir -p /mnt/$DIR_NAME
    mount -t xfs -o logbufs=8,logbsize=256k,osyncisdsync,barrier,largeio,noatime,nodiratime,inode64 /dev/etherd/e0.$i /mnt/$DIR_NAME
    
    mkdir /mnt/$DIR_NAME/populating
    for j in `seq 0 255`; do
        SUB_DIR_NAME=`printf '%02x' $j`
        mkdir /mnt/$DIR_NAME/populating/$SUB_DIR_NAME
    done

    mkdir /mnt/$DIR_NAME/live
    for j in `seq 0 255`; do
        SUB_DIR_NAME=`printf '%02x' $j`
        mkdir /mnt/$DIR_NAME/live/$SUB_DIR_NAME
    done
done
