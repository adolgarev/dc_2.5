#ifndef CMD_H_
#define CMD_H_


#include "request.h"

typedef int (*cmd)(request_s*);

extern cmd cmds[];


#endif //CMD_H_