
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "log.h"
#include "util.h"

//-------------------------------------------------------------------

void list_new(list_s *l) {
    l->next = l;
    l->prev = l;
}
//-------------------------------------------------------------------

void list_add(list_s *n, list_s *p) {
    p->next->prev = n;
    n->next = p->next;
    p->next = n;
    n->prev = p;
}
//-------------------------------------------------------------------

void list_add_tail(list_s *n, list_s *p) {
    p->prev->next = n;
    n->prev = p->prev;
    p->prev = n;
    n->next = p;
}
//-------------------------------------------------------------------

void list_del(list_s *p) {
    if (p->prev)
        p->prev->next = p->next;
    if (p->next)
        p->next->prev = p->prev;
    p->prev = NULL;
    p->next = NULL;
}
//-------------------------------------------------------------------

int setnonblocking(int fd) {
    int flags;

    if ((flags = fcntl(fd, F_GETFL, 0)) == -1) {
        log_syserr("fcntl");
        return -1;
    }
    if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
        log_syserr("fcntl");
        return -1;
    }

    return 0;
}
//-------------------------------------------------------------------
