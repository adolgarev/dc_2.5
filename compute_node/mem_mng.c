
#include <stdlib.h>
#include <stddef.h>

#include "mem_mng.h"
#include "config.h"
#include "util.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "log.h"

static data_block_s *alloc_block(mem_mng_s *mng);
static void free_block(data_block_s *block);
//------------------------------------------------------------------

mem_mng_s *init_mem_mng(long block_size, long prealloc_num) {

    mem_mng_s *new_mng = malloc(sizeof(mem_mng_s));
    if (!new_mng) {
        log_syserr("cannot allocate memory");
        goto err;
    }
    memset(new_mng, 0, sizeof(mem_mng_s));

    new_mng->block_size = block_size;
    new_mng->prealloc = prealloc_num;

    list_new(&new_mng->free_blocks);
    list_new(&new_mng->used_blocks);

    for(int i = 0 ; i < prealloc_num ; i++) {
        data_block_s *new_block = alloc_block(new_mng);
        if (!new_block)
            goto err;
        list_add_tail(&new_block->data_blocks_list, &new_mng->free_blocks);
    }

    new_mng->free_blocks_num = prealloc_num;
    return new_mng;

err:
    deinit_mem_mng(new_mng);
    return 0;
}
//------------------------------------------------------------------

static data_block_s *alloc_block(mem_mng_s *mng) {
    data_block_s *new_block;
    new_block = malloc(sizeof(data_block_s) + mng->block_size);
    if (!new_block) {
        log_syserr("cannot allocate memory");
        goto err;
    }

    return new_block;

err:
    return NULL;
}
//------------------------------------------------------------------

static void free_block(data_block_s *block) {
    if(block) {
        free(block);
    }
}
//------------------------------------------------------------------

void deinit_mem_mng(mem_mng_s *mng) {
    list_s *elt;
    data_block_s *block;
    if (mng) {
        for (elt = mng->free_blocks.next; elt != &mng->free_blocks; elt = elt->next) {
            block = list_elt(mng->free_blocks.next, data_block_s, data_blocks_list);
            list_del(&block->data_blocks_list);
            free_block(block);
        }
        for (elt = mng->used_blocks.next; elt != &mng->used_blocks; elt = elt->next) {
            block = list_elt(mng->used_blocks.next, data_block_s, data_blocks_list);
            list_del(&block->data_blocks_list);
            free_block(block);
        }
        free(mng);
    }
}
//------------------------------------------------------------------

void *alloc_data(mem_mng_s *mng) {
    data_block_s *block;
    if (mng->free_blocks.next != &mng->free_blocks) {
        block = list_elt(mng->free_blocks.next, data_block_s, data_blocks_list);
        list_del(&block->data_blocks_list);
        mng->free_blocks_num--;
    } else {
        block = alloc_block(mng);
        if (!block)
            return NULL;
    }
    list_add_tail(&block->data_blocks_list, &mng->used_blocks);
    return block->data;
}
//------------------------------------------------------------------

int free_data(mem_mng_s *mng, void *data) {
    if (mng->used_blocks.next != &mng->used_blocks) {
        data_block_s *block = (data_block_s*)((char*)data - offsetof(data_block_s, data));
        list_del(&block->data_blocks_list);
        if (mng->free_blocks_num >= mng->prealloc) {
            free_block(block);
        } else {
            list_add_tail(&block->data_blocks_list, &mng->free_blocks);
            mng->free_blocks_num++;
        }
        return 0;
    } else {
        log_err("unable to free block");
        return 1;
    }
}
//------------------------------------------------------------------
