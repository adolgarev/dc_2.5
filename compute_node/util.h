#ifndef UTIL_H_
#define UTIL_H_

#include <stddef.h>

typedef struct list_s_ {
    struct list_s_ *prev;
    struct list_s_ *next;
} list_s;

// Init new two-way list
void list_new(list_s *l);
// Add n after p
void list_add(list_s *n, list_s *p);
// Add n before p
void list_add_tail(list_s *n, list_s *p);
// Remove p
void list_del(list_s *p);

#define list_elt(p, t, m)       \
    ((void*)((char*)p - offsetof(t, m)))

//-------------------------------------------------------------------
int setnonblocking(int fd);

//-------------------------------------------------------------------

#endif //UTIL_H_
