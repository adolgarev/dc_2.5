#define _XOPEN_SOURCE   600
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ev.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <signal.h>

#include "config.h"
#include "worker.h"
#include "log.h"
#include "request.h"
#include "mem_mng.h"


static void on_new_connection(struct ev_loop *loop, ev_io *w, int revents);
static void on_new_message(struct ev_loop *loop, ev_io *w, int revents);
int connection_flush(connection_s *con);
static void on_write(struct ev_loop *loop, ev_io *w, int revents);
static void connection_del(connection_s *con);
static request_s *request_new(connection_s *con);
static void request_del(request_s *req);
static int listen_sock_create();
static int init_workers();
static void on_response(struct ev_loop *loop, ev_io *w, int revents);

static int serversd = -1;
static struct ev_loop *e_loop;
static list_s request_wait;
static ev_io response_watcher;
static char version = '\1';
static int num_reqs;
static list_s connection_list;
static mem_mng_s *request_mem_mng;
static mem_mng_s *data_mem_mng;

//------------------------------------------------------------------

static void connection_del(connection_s *con) {
    list_s *elt;
    request_s *req;

    list_del(&con->connection_list);
    
    for (elt = con->request_list.next; elt != &con->request_list; ) {
        req = list_elt(elt, request_s, request_list);
        elt = elt->next;
        if (!req->processing)
            request_del(req);
        else {
            req->recycled = 1;
            list_del(&req->request_list);
            list_del(&req->request_wait);
        }
    }

    if (con->writing) {
        ev_io_stop(e_loop, &con->write_watcher);
    }
    ev_io_stop(e_loop, &con->read_watcher);
    if (con->sd != -1) {
        close(con->sd);
    }
    log_debug("Socket %i was closed", con->sd);
    
    free(con);
}
//------------------------------------------------------------------

static request_s *request_new(connection_s *con) {
    request_s *new_request;

    new_request = alloc_data(request_mem_mng);
    if (!new_request) {
        log_err("cannot allocate memory");
        goto err;
    }
    memset(new_request, 0, sizeof(request_s));
    
    // Add to connection's list of requests
    list_add_tail(&new_request->request_list, &con->request_list);
    
    new_request->con = con;
    
    {
        // Flow control
        num_reqs++;
        
        if (num_reqs == REQUEST_HIGH_WATERMARK) {
            list_s *elt;
            connection_s *con;
            for (elt = connection_list.next; elt != &connection_list; elt = elt->next) {
                con = list_elt(elt, connection_s, connection_list);
                ev_io_stop(e_loop, &con->read_watcher);
            }
        }
    
    }
    
    return new_request;
err:
    return NULL;
}
//------------------------------------------------------------------

static void request_del(request_s *req) {
    list_del(&req->request_list);
    list_del(&req->request_wait);

    if (req->data)
        free_data(data_mem_mng, req->data);

    free_data(request_mem_mng, req);

    {
        // Flow control
        num_reqs--;
        
        if (num_reqs == REQUEST_LOW_WATERMARK) {
            list_s *elt;
            connection_s *con;
            for (elt = connection_list.next; elt != &connection_list; elt = elt->next) {
                con = list_elt(elt, connection_s, connection_list);
                ev_io_start(e_loop, &con->read_watcher);
            }
        }
    
    }
}
//------------------------------------------------------------------

int main(int argc, char *argv[]) {
    ev_io server_socket_watcher;
    
    // Ignore SIGPIPE on writing to the closed socket
    signal(SIGPIPE, SIG_IGN);
    
    
    e_loop = ev_default_loop(EVBACKEND_EPOLL);
    
    list_new(&request_wait);
    list_new(&connection_list);
    
    if (init_workers())
        goto err;

    request_mem_mng = init_mem_mng(sizeof(request_s), PREALLOC_REQ_NUM);
    if (!request_mem_mng)
        goto err;

    data_mem_mng = init_mem_mng(DATA_LEN, PREALLOC_DATA_NUM);
    if (!data_mem_mng)
        goto err;
    
    if ((serversd = listen_sock_create()) == -1)
        goto err;


    ev_io_init(&server_socket_watcher, on_new_connection, serversd, EV_READ);
    ev_io_start(e_loop, &server_socket_watcher);

    ev_loop(e_loop, 0);

    deinit_mem_mng(request_mem_mng);
    deinit_mem_mng(data_mem_mng);
    return 0;
err:
    deinit_mem_mng(request_mem_mng);
    deinit_mem_mng(data_mem_mng);
    if (serversd != -1)
        close(serversd);
    return 1;
}
//-------------------------------------------------------------------

static void on_new_connection(struct ev_loop *loop, ev_io *w, int revents) {
    int newsd = -1;
    ev_io *client_socket_watcher = NULL;
    connection_s* newc_data = NULL;

    newsd = accept(serversd, NULL, NULL);
    if (newsd == -1) {
        log_err("cannot accept new connection");
        goto err;
    }

    log_debug("New connection established, socket descriptor %i", newsd);

    if (setnonblocking(newsd))
        goto err;

    newc_data = malloc(sizeof(connection_s));
    if (!newc_data) {
        log_syserr("cannot allocate memory");
        goto err;
    }
    memset(newc_data, 0, sizeof(connection_s));
    newc_data->sd = newsd;
    
    list_new(&newc_data->request_list);

    client_socket_watcher = &newc_data->read_watcher;
    client_socket_watcher->data = newc_data;
    ev_io_init(client_socket_watcher, on_new_message, newsd, EV_READ);

    ev_io_start(loop, client_socket_watcher);
    
    list_add_tail(&newc_data->connection_list, &connection_list);

    return;
err:
    if (newsd != -1)
        close(newsd);
    free(newc_data);
}
//-------------------------------------------------------------------

static void on_new_message(struct ev_loop *loop, ev_io *w, int revents) {
#define RCVBUFLEN       2000
    char buf[RCVBUFLEN];
    connection_s *con = (connection_s*)w->data;
    request_s *new_request = NULL;
    int send_req_res;
    
    if (con->request_list.prev != &con->request_list)
        new_request = list_elt(con->request_list.prev, request_s, request_list);
    do {
        long nbytes;
        char *p;
        
        nbytes = read(con->sd, buf, sizeof(buf));
        if (nbytes == -1 && (EAGAIN == errno || EWOULDBLOCK == errno)) {
            // nothing was read. Wait for another event
            return;
        } else if (nbytes == -1 && EINTR == errno) {
            // interrupted system call, try again
            continue;
        } else if (nbytes == -1) {
            // error happened. Stop all watchers and close socket
            log_err("cannot read from socket %i", con->sd);
            goto err;
        } else if (nbytes == 0) {
            // end of file - socket was closed
            log_debug("Connection on socket %i was closed by client", con->sd);
            goto err;
        }
        
        if (!new_request && (new_request = request_new(con)) == NULL) {
            goto err;
        }
        
        for (p = buf; p < buf + nbytes; p++) {
            
            if (new_request->state == request_state_end && (new_request = request_new(con)) == NULL)
                goto err;
            
            
            switch ( new_request->state ) {
                case request_state_version:
                    new_request->state = request_state_command;
                    break;
                case request_state_command:
                    new_request->command = *p;
                    if (new_request->command == command_put || new_request->command == command_get) {
                        new_request->data = alloc_data(data_mem_mng);
                        if (!new_request->data) {
                            log_err("cannot allocate memory");
                            goto err;
                        }
                    }
                    new_request->state = request_state_key;
                    break;
                case request_state_key:
                    new_request->key[new_request->off++] = *p;
                    if (new_request->off >= KEY_LEN) {
                        new_request->off = 0;
                        if (new_request->command == command_put)
                            new_request->state = request_state_gen_number;
                        else
                            new_request->state = request_state_end;
                    }
                    break;
                case request_state_gen_number:
                    new_request->gen_number[new_request->off++] = *p;
                    if (new_request->off >= GEN_NUM_LEN) {
                        new_request->off = 0;
                        new_request->state = request_state_data;
                    }
                    break;
                case request_state_data:
                    {
                        size_t avail, need, copy;
                        
                        avail = nbytes - (p - buf);
                        need = DATA_LEN - new_request->off;
                        copy = need < avail ? need : avail;
                        
                        memcpy(new_request->data + new_request->off, p, copy);
                        new_request->off += copy;
                        p += copy - 1;

                        if (new_request->off >= DATA_LEN) {
                            new_request->off = 0;
                            new_request->state = request_state_end;
                        }
                        break;
                    }
                default:
                    log_err("Wrong request state %d", new_request->state);
                    goto err;
            }
            
            if (new_request->state == request_state_end) {
                // Add request
                send_req_res = send_request(new_request);
                if (chan_busy == send_req_res) {
                    list_add_tail(&new_request->request_wait, &request_wait);
                } else if (chan_err == send_req_res) {
                    goto err;
                } else {
                    new_request->processing = 1;
                }
            }
        }
    } while(0);

    return;
err:
    connection_del(con);
}
//-------------------------------------------------------------------

int connection_flush(connection_s *con) {
    list_s *elt;
    request_s *req;
    
    ssize_t         n, n_backup;
#define IOVECSIZE   4000
    struct iovec    iov[IOVECSIZE];
    struct iovec   *p1;
    struct iovec   *p2;
    int             iovc = 0;
    
    p1 = iov;
    for (elt = con->request_list.next; elt != &con->request_list; elt = elt->next) {
        req = list_elt(elt, request_s, request_list);
        
        if (!req->finished)
            break;
        
        if (!req->iovp) {
            req->iovp = req->iov;
            switch (req->command) {
                case command_get:
                    req->iov[3].iov_base = req->data;
                    req->iov[3].iov_len = DATA_LEN;
                    req->iovc = 4;
                case command_head:
                    req->iov[2].iov_base = req->gen_number;
                    req->iov[2].iov_len = GEN_NUM_LEN;
                    if (!req->iovc)
                        req->iovc = 3;
                case command_put:
                    req->iov[0].iov_base = &version;
                    req->iov[0].iov_len = 1;
                    req->iov[1].iov_base = &req->code;
                    req->iov[1].iov_len = 1;
                    if (!req->iovc)
                        req->iovc = 2;
                    break;
            }
        }
        
        for (p2 = req->iovp; p1 < iov + IOVECSIZE && p2 < req->iovp + req->iovc; p1++, p2++) {
            p1->iov_base = p2->iov_base;
            p1->iov_len = p2->iov_len;
            iovc++;
        }
        
        if (p2 >= iov + IOVECSIZE)
            break;
    }
    
    p1 = iov;
    while (iovc) {
        n = writev(con->sd, p1, iovc);
        if (n == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK)
                goto again;
            else if (errno == EINTR)
                continue;

            log_syserr("writev");
            goto err;
        }

        n_backup = n;
        for (elt = con->request_list.next; elt != &con->request_list; ) {
            req = list_elt(elt, request_s, request_list);
            elt = elt->next;
            
            while (n) {
                if (req->iovp->iov_len <= n) {
                    n -= req->iovp->iov_len;
                    req->iovp++;
                    req->iovc--;
                    
                    if (!req->iovc) {
                        // All data sent
                        request_del(req);
                        break;
                    }
                }
                else {
                    req->iovp->iov_base  += n;
                    req->iovp->iov_len   -= n;
                    n = 0;
                }
            }
            
            if (!n)
                break;
        }

        n = n_backup;
        p2 = p1;
        while (p2 < p1 + iovc) {
            if (p2->iov_len <= n) {
                n -= p2->iov_len;
                p2++;
            }
            else {
                p2->iov_base  += n;
                p2->iov_len   -= n;
                break;
            }
        }

        if (p2 == p1 + iovc)
            break;
        else {
            iovc -= p2 - p1;
            p1 = p2;
        }
    }

    if (con->writing) {
        ev_io_stop(e_loop, &con->write_watcher);
        con->writing = 0;
    }
    return 0;
again:
    if (!con->writing) {
        ev_io_init(&con->write_watcher, on_write, con->sd, EV_WRITE);
        ev_io_start(e_loop, &con->write_watcher);
        con->writing = 1;
    }
    return 0;
err:
    connection_del(con);
    return 1;
}
//-------------------------------------------------------------------

static void on_write(struct ev_loop *loop, ev_io *w, int revents) {
    connection_s *con = w->data;
    connection_flush(con);
}
//-------------------------------------------------------------------

static int init_workers() {
    int res_fd;

    res_fd = init_workers_io();
    if(!res_fd) {
        log_err("cannot init workers' io");
        goto err;
    }

    // Start watcher on response pipe
    ev_io_init(&response_watcher, on_response, res_fd, EV_READ);
    ev_io_start(e_loop, &response_watcher);

    for (int i = 0 ; i < WORKERS_NUM ; i++) {
        if ((start_worker())) {
            log_err("cannot start worker %i", i);
            goto err;
        }
    }
    
    return 0;
err:
    return 1;
}
//-------------------------------------------------------------------

static void on_response(struct ev_loop *loop, ev_io *w, int revents) {
    connection_s *con;
    request_s *response;
    int read_resp_res, send_req_res;
    do {
        read_resp_res = read_response(&response);
        if (chan_err == read_resp_res) {
            goto error;
        } else if (chan_empty == read_resp_res) {
            break;
        }
        response->finished = 1;
        response->processing = 0;

        if (response->recycled) {
            request_del(response);
            continue;
        }

        con = response->con;
        connection_flush(con);
    } while (1);

    while (request_wait.next != &request_wait) {
        request_s *request = list_elt(request_wait.next, request_s, request_wait);
        send_req_res = send_request(request);
        if (chan_busy == send_req_res) {
            break;
        } else if (chan_err == send_req_res) {
            goto error;
        } else {
            list_del(&request->request_wait);
            request->processing = 1;
        }
    }
error:
    return;
}

//-------------------------------------------------------------------

int listen_sock_create() {
    int sockfd = 0;
    int yes = 1;
    int ret = 0;
    struct addrinfo hints;
    struct addrinfo *servinfo = NULL, *p = NULL;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((ret = getaddrinfo(HOST, PORT, &hints, &servinfo)) != 0) {
        log_err("getaddrinfo: %s", gai_strerror(ret));
        goto error;
    }
    
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
            log_syserr("socket");
            goto err_loop;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
            log_syserr("setsockopt");
            goto error;
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            log_syserr("socket");
            goto err_loop;
        }

        break;
    err_loop:
        if (sockfd != -1)
            close(sockfd);
    }
    
    if (p == NULL)  {
        log_err("failed to bind");
        goto error;
    }

    freeaddrinfo(servinfo);
    servinfo = NULL;
    
    if (listen(sockfd, BACKLOG) == -1) {
        log_syserr("listen");
        goto error;
    }
    
    
    return sockfd;
error:
    if (servinfo)
        freeaddrinfo(servinfo);
    return -1;
}

//-------------------------------------------------------------------
