#ifndef MEM_MNG_H_
#define MEM_MNG_H_

#include "util.h"

typedef struct _data_block_s {
    list_s data_blocks_list;
    char data[0];
} data_block_s;

typedef struct _mem_mng_s {
    list_s free_blocks;
    list_s used_blocks;
    long free_blocks_num;
    long prealloc;
    long block_size;
} mem_mng_s;

mem_mng_s *init_mem_mng(long block_size, long plealloc_num);
void deinit_mem_mng(mem_mng_s *mng);
void *alloc_data(mem_mng_s *mng);
int free_data(mem_mng_s *mng, void *data);

#endif //MEM_MNG_H_
