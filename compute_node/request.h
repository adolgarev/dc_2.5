#ifndef REQUEST_H_
#define REQUEST_H_

#include <sys/uio.h>
#include <ev.h>

#include "util.h"

#define DATA_LEN 16384
#define KEY_LEN 20
#define GEN_NUM_LEN 8

typedef enum {
    command_head,
    command_put,
    command_get
} command_e;

typedef enum {
    code_ok,
    code_not_found,
    code_error
} code_e;

typedef enum {
    request_state_version,
    request_state_command,
    request_state_key,
    request_state_gen_number,
    request_state_data,
    request_state_end
} request_state_e;

//-------------------------------------------------------------------

struct _connection_s;
typedef struct _request_s {
    // Request-response data
    command_e command;
    unsigned char key[KEY_LEN];
    char gen_number[GEN_NUM_LEN];
    char *data;
    code_e code;
    
    // Auxiliary data to track read progress from socket
    size_t off;
    request_state_e state;
    
    // Auxiliary data to track write progress to socket
    struct iovec    iov[4];
    struct iovec   *iovp;
    int             iovc;
    
    // List of all requests in connection
    list_s request_list;
    struct _connection_s *con;
    // List of wait requests
    list_s request_wait;

    // True if in processing by worker
    unsigned processing:1;
    // True if request was processed
    unsigned finished:1;
    // True if connection was closed
    // but request is on worker side
    // and must be freed later
    unsigned recycled:1;
} request_s;

//-------------------------------------------------------------------

typedef struct _connection_s {
    int sd;
    
    list_s request_list;
    list_s connection_list;
    
    ev_io write_watcher;
    ev_io read_watcher;
    
    // True if write watcher started
    unsigned writing:1;
} connection_s;

//-------------------------------------------------------------------
#endif //REQUEST_H_
