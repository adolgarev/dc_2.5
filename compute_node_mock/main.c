#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ev.h>
#include <time.h>
#include <fcntl.h>
#include "protocol.h"
#include "log.h"

#define PORT 4000
#define BACKLOG 10

#define PUT_DATA_LEN (DATA_LEN + GEN_NUM_LEN)

typedef struct _message_s {
    void* message;
    int offset;
    int len;
    struct _message_s *next;
} message_s;

typedef struct _connection_s {
    int sd;
    int req_part_rcvd;
    int put_part_rcvd;
    char req_buf[REQUEST_LEN + 1];
    char put_data_buf[PUT_DATA_LEN + 1];
    message_s *head;
    message_s *tail;
    ev_io *ready_to_write_watcher;
    ev_io *ready_to_read_watcher;
    struct _connection_s *next;
    struct _connection_s *prev;
} connection_s;
//-------------------------------------------------------------------

static void start_server();
static void on_new_message (struct ev_loop *loop, ev_io *w, int revents);
static void on_new_connection (struct ev_loop *loop, ev_io *w, int revents);
void send_message(connection_s *c_data, void *message, int len);
void on_put_request(connection_s *c_data);
void on_head_request(connection_s *c_data);
void on_get_request(connection_s *c_data);
static void on_ready_to_write(struct ev_loop *loop, ev_io *w, int revents);

static int serversd = 0;
static struct ev_loop *e_loop;
static connection_s *connections_head = 0;
static connection_s *connections_tail = 0;
//-------------------------------------------------------------------

static void error() {
    connection_s *current = connections_head, *next;
    while (current) {
        if (current->sd) {
            close( current->sd );
        }
        next = current->next;
        free(current);
        current = next;
    }
    if (serversd) {
        close(serversd);
    }
}

//------------------------------------------------------------------

static void client_socket_error(connection_s *c_data) {
    message_s *message = c_data->head, *next_message;

    if (c_data->ready_to_write_watcher) {
        ev_io_stop (e_loop, c_data->ready_to_write_watcher);
        free(c_data->ready_to_write_watcher);
    }
    if(c_data->ready_to_read_watcher) {
        ev_io_stop (e_loop, c_data->ready_to_read_watcher);
    }
    if (c_data->sd) {
        close(c_data->sd);
    }
    log_info("Socket %i was closed", c_data->sd);

    while (message) {
        next_message = message->next;
        free(message->message);
        free(message);
        message = next_message;
    }

    free(c_data);
}
//------------------------------------------------------------------

int main(int argc, char *argv[]) {
    start_server();
    return 0;
}
//-------------------------------------------------------------------

static void start_server()
{
    int reuse_opt = 1;
    struct sockaddr_in serveraddr;
    ev_io server_socket_watcher;

    serversd = socket(AF_INET, SOCK_STREAM, 0);
    if (serversd  < 0) {
        error();
        plog_syserr("cannot create server socket");
    }

    if (setsockopt(serversd, SOL_SOCKET, SO_REUSEADDR, &reuse_opt, sizeof(reuse_opt)) < 0) {
        error();
        plog_syserr("cannot set socket options");
    }

    memset(&serveraddr, 0, sizeof(struct sockaddr_in));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = INADDR_ANY;
    serveraddr.sin_port = htons(PORT);

    if(bind(serversd, (struct sockaddr *)&serveraddr, sizeof(struct sockaddr_in)) < 0) {
        error();
        plog_syserr("cannot bind server socket to port %i", PORT);
    }

    if(listen(serversd, BACKLOG) < 0) {
        error();
        plog_syserr("cannot start listening");
    }

    e_loop = ev_default_loop(EVBACKEND_EPOLL);

    ev_io_init(&server_socket_watcher, on_new_connection, serversd, EV_READ);
    ev_io_start(e_loop, &server_socket_watcher);

    ev_loop(e_loop, 0);
}
//-------------------------------------------------------------------

static void on_new_connection (struct ev_loop *loop, ev_io *w, int revents) {
    struct sockaddr_in clientaddr;
    socklen_t addrlen = sizeof(struct sockaddr_in);
    int newsd, flags;
    ev_io *client_socket_watcher;
    connection_s* newc_data;

    newsd = accept(serversd, (struct sockaddr *)&clientaddr, &addrlen);
    if(newsd == -1) {
        plog_err("cannot accept new connection");
    }
    else
    {
        log_info("new connection established, socket descriptor %i", newsd);

        flags = fcntl(newsd, F_GETFL, 0);
        fcntl(newsd, F_SETFL, flags | O_NONBLOCK);

        newc_data = malloc(sizeof(connection_s));
        if(!newc_data) {
            error();
            plog_syserr("cannot allocate memory");
        }
        if (!connections_head) {
            connections_head = connections_tail = newc_data;
        } else {
            connections_tail->next = newc_data;
            newc_data->prev = connections_tail;
            connections_tail = newc_data;
        }
        memset(newc_data, 0, sizeof(connection_s));
        newc_data->sd = newsd;
        newc_data->put_part_rcvd = PUT_DATA_LEN;

        client_socket_watcher = malloc(sizeof(ev_io));
        if(!client_socket_watcher) {
            error();
            plog_syserr("cannot allocate memory");
        }
        client_socket_watcher->data = newc_data;
        ev_io_init(client_socket_watcher, on_new_message, newsd, EV_READ);
        ev_io_start(loop, client_socket_watcher);
        newc_data->ready_to_read_watcher = client_socket_watcher;
    }
}
//-------------------------------------------------------------------

static void on_new_message (struct ev_loop *loop, ev_io *w, int revents) {
    connection_s *c_data = (connection_s *)w->data;
    long nbytes;

    // if PUT data is being received from client on this socket -
    // try to read it all to another buffer
    if (PUT_DATA_LEN != c_data->put_part_rcvd) {
        nbytes = read(c_data->sd, c_data->put_data_buf + c_data->put_part_rcvd, PUT_DATA_LEN - c_data->put_part_rcvd);
        if( nbytes == -1 && EAGAIN == errno ) {
            // nothing was read. Wait for another event
            return;
        } else if (nbytes == -1) {
            // error happened. Stop all watchers and close socket
            plog_err("cannot read from socket %i", c_data->sd);
            client_socket_error(c_data);
        } else if (nbytes == 0) {
            // end of file - socket was closed
            log_info("connection on socket %i was closed by client", c_data->sd);
            client_socket_error(c_data);
        } else {
            c_data->put_part_rcvd += nbytes;
            if(PUT_DATA_LEN == c_data->put_part_rcvd) {
                on_put_request(c_data);
            }
        }
    } else {
        // if PUT data is NOT being received from client on this socket -
        // it is new request
        nbytes = read(c_data->sd, c_data->req_buf + c_data->req_part_rcvd, REQUEST_LEN - c_data->req_part_rcvd);
        if( nbytes == -1 && EAGAIN == errno ) {
            // nothing was read. Wait for another event
            return;
        } else if (nbytes == -1) {
            // error happened. Stop all watchers and close socket
            plog_err("cannot read from socket %i", c_data->sd);
            client_socket_error(c_data);
        } else if (nbytes == 0) {
            // end of file - socket was closed
            log_info("connection on socket %i was closed by client", c_data->sd);
            client_socket_error(c_data);
        } else {
            c_data->req_part_rcvd += nbytes;
            if(c_data->req_part_rcvd == REQUEST_LEN) {
                c_data->req_part_rcvd = 0;
                head_request_s *req = (head_request_s *)c_data->req_buf;
                switch ( req->command ) {
                case COMMAND_PUT:
                    c_data->put_part_rcvd = 0;
                    break;
                case COMMAND_HEAD:
                    on_head_request(c_data);
                    break;
                case COMMAND_GET:
                    on_get_request(c_data);
                    break;
                default:
                    log_err("Command %i is not supported", req->command);
                }
            }
        }
    }
}
//-------------------------------------------------------------------

void on_put_request(connection_s *c_data) {
    put_response_s *response_message;
    response_message = malloc(PUT_RESPONSE_LEN);
    memset(response_message, 0, PUT_RESPONSE_LEN);
    send_message(c_data, response_message, PUT_RESPONSE_LEN);
    memset(c_data->req_buf,0,sizeof(c_data->req_buf));
}
//-------------------------------------------------------------------

void on_head_request(connection_s *c_data) {
    head_response_s *response_message;
    response_message = malloc(HEAD_RESPONSE_LEN);
    memset(response_message, 0, HEAD_RESPONSE_LEN);
    memcpy(response_message->gen_number, c_data->put_data_buf, GEN_NUM_LEN);
    send_message(c_data, response_message, HEAD_RESPONSE_LEN);
    memset(c_data->req_buf,0,sizeof(c_data->req_buf));
}
//-------------------------------------------------------------------

void on_get_request(connection_s *c_data) {
    get_response_s *response_message;
    response_message = malloc(GET_RESPONSE_LEN);
    memset(response_message, 0, GET_RESPONSE_LEN);
    memcpy(response_message->gen_number, c_data->put_data_buf, GEN_NUM_LEN);
    memcpy(response_message->data, c_data->put_data_buf + GEN_NUM_LEN, DATA_LEN);
    send_message(c_data, response_message, GET_RESPONSE_LEN);
    memset(c_data->req_buf,0,sizeof(c_data->req_buf));
}
//-------------------------------------------------------------------

void send_message(connection_s *c_data, void *message, int len) {
    int nbytes;
    ev_io *ready_to_write_watcher;
    message_s *new_message;

    if(NULL == c_data->head) {
        nbytes = write(c_data->sd, message, len);
        if( (nbytes == -1 && EAGAIN == errno) || (nbytes >= 0 && nbytes != len)) {
            new_message = malloc (sizeof(message_s));
            if(!new_message) {
                error();
                plog_syserr("can't allocate memory");
            }
            new_message->message = message;
            new_message->next = NULL;
            new_message->offset = (nbytes == -1) ? 0 : nbytes;
            new_message->len = len;
            c_data->head = c_data->tail = new_message;
            ready_to_write_watcher = malloc(sizeof(ev_io));
            ready_to_write_watcher->data = c_data;
            ev_io_init(ready_to_write_watcher, on_ready_to_write, c_data->sd, EV_WRITE);
            ev_io_start(e_loop, ready_to_write_watcher);
            c_data->ready_to_write_watcher = ready_to_write_watcher;
        } else if (nbytes == -1) {
            plog_err("cannot write to socket %i", c_data->sd);
            client_socket_error(c_data);
        } else {
            free(message);
        }
    } else {
        new_message = malloc (sizeof(message_s));
        if(!new_message) {
            error();
            plog_syserr("can't allocate memory");
        }
        new_message->message = message;
        new_message->next = NULL;
        new_message->offset = 0;
        new_message->len = len;
        c_data->tail->next = new_message;
    }
}
//-------------------------------------------------------------------

static void on_ready_to_write(struct ev_loop *loop, ev_io *w, int revents) {
    int nbytes, len;
    connection_s *c_data = w->data;
    message_s *message = c_data->head, *next_message;

    while (message) {
        len = message->len - message->offset;
        nbytes = write(c_data->sd, message->message + message->offset, len);
        if( (nbytes == -1 && EAGAIN == errno) || (nbytes >= 0 && nbytes != len)) {
            message->offset += (nbytes == -1) ? 0 : nbytes;
            c_data->head = message;
            break;
        } else if (nbytes == -1) {
            plog_err("cannot write to socket %i", c_data->sd);
            client_socket_error(c_data);
            break;
        } else {
            next_message = message->next;
            free(message->message);
            free(message);
            message = next_message;
        }
    }

    if (NULL == message) {
        c_data->head = NULL;
        c_data->ready_to_write_watcher = 0;
        ev_io_stop (loop, w);
        free(w);
    }
}

//-------------------------------------------------------------------
