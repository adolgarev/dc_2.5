#ifndef RAND_DATA_H
#define RAND_DATA_H

int rd_init(long n);
void rd_close();
int rd_get(long len, char *data);

#endif //RAND_DATA_H
