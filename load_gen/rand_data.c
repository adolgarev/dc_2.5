
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "rand_data.h"
#include "log.h"

#define DATA_SHIFT 10

static int urandom_fd = 0;
char *buf;
long size;
long i;

int rd_init(long n) {
    size = n;
    i = size;
    buf = malloc(size);
    if (!buf) {
        log_syserr("unable to allocate memory");
        return 1;
    }
    urandom_fd = open("/dev/urandom", O_RDONLY);
    if(!urandom_fd) {
        log_syserr("unable to open /dev/urandom");
        return 1;
    }
    return 0;
}

int rd_get(long len, char *data) {
    if (i + len >= size) {
        if(!read(urandom_fd, buf, size)) {
            return 1;
        }
        i = 0;
    }
    memcpy(data, buf + i, len);
    i += DATA_SHIFT;
    return 0;
}

void rd_close() {
    free(buf);
    close(urandom_fd);
}
