
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ev.h>
#include <time.h>
#include <fcntl.h>
#include <openssl/sha.h>
#include "protocol.h"
#include "log.h"
#include "rand_data.h"

#define HEAD_REQUESTS_NUM_DEF 72000
#define PUT_REQUESTS_NUM_DEF 3000
#define SOCK_NUM_DEF 2000
#define TIMER_ELAPSES_IN_SECOND_DEF 300
#define PORT_DEF 4000
#define HOST_DEF "localhost"

static long HEAD_REQUESTS_NUM = HEAD_REQUESTS_NUM_DEF;
static long PUT_REQUESTS_NUM = PUT_REQUESTS_NUM_DEF;
static long SOCK_NUM = SOCK_NUM_DEF;
static long TIMER_ELAPSES_IN_SECOND = TIMER_ELAPSES_IN_SECOND_DEF;

static unsigned int PORT = PORT_DEF;
static char *HOST = HOST_DEF;

typedef struct _message_s {
    void* message;
    int offset;
    int len;
    struct _message_s *next;
} message_s;

typedef struct _next_response_s {
    int len;
    struct _next_response_s *next;
} next_response_s;

typedef struct _connection_data_s {
    int sd;
    int resp_part_rcvd;
    // number of all messages that are sent
    // and received via this socket
    long req_sent;
    long resp_recv;
    long queued;
    long dequeued;
    message_s *req_head;
    message_s *req_tail;
    next_response_s *res_head;
    next_response_s *res_tail;
    int next_res_len;
    ev_io *ready_to_write_watcher;
    ev_io *ready_to_read_watcher;
} connection_data_s;

typedef struct _timer_data_s {
    int head_last_socket;
    int put_last_socket;
    // number of messages to send on timeout
    int put_mess_num;
    int head_mess_num;
    int last_time;
} timer_data_s;

static void socket_error(connection_data_s *c_data);
static void parse_opts(int argc, char **argv);
static void usage();
static int open_socket();
static void on_timeout(struct ev_loop *loop, ev_timer *w, int revents);
static void add_next_response_len(connection_data_s *c_data, int len);
static void get_next_response_len(connection_data_s *c_data);
static void *head_request();
static void *put_request();
static void send_message(connection_data_s *c_data, void *message, int len);
static void on_ready_to_write(struct ev_loop *loop, ev_io *w, int revents);
static void on_responce(struct ev_loop *loop, ev_io *w, int revents);

static struct ev_loop *e_loop;
static ev_timer timeout_watcher = {0};
static timer_data_s timer_data = {0};
static connection_data_s *connections = 0;
static ev_io *response_watchers = 0;

//-------------------------------------------------------------------

#define error(fmt, ...)       \
    on_error(); \
    plog_syserr(fmt, ##__VA_ARGS__)

//------------------------------------------------------------------

static void on_error() {
    ev_timer_stop (e_loop, &timeout_watcher);
    if(connections) {
        for (int _i = 0 ; _i < SOCK_NUM ; _i++) {
            socket_error(&connections[_i]);
        }
        free(connections);
    }
    if(response_watchers) {
        free(response_watchers);
    }
    rd_close();
}
//-------------------------------------------------------------------

static void socket_error(connection_data_s *c_data) {
    message_s *message = c_data->req_head, *next_message;
    next_response_s *next_res = c_data->res_head, *res;
    if (c_data->ready_to_write_watcher) {
        ev_io_stop (e_loop, c_data->ready_to_write_watcher);
        free(c_data->ready_to_write_watcher);
    }
    if(c_data->ready_to_read_watcher) {
        ev_io_stop (e_loop, c_data->ready_to_read_watcher);
    }
    while (message) {
        next_message = message->next;
        free(message->message);
        free(message);
        message = next_message;
    }
    while (next_res) {
        res = next_res->next;
        free(next_res);
        next_res = res;
    }
    if (c_data->sd) {
        close(c_data->sd);
    }
    log_info("Socket %i was closed", c_data->sd);
    c_data->sd = 0;
}
//-------------------------------------------------------------------

int main(int argc, char **argv) {
    e_loop = ev_default_loop(EVBACKEND_EPOLL);

    parse_opts(argc, argv);

    if(rd_init(PUT_REQUESTS_NUM * DATA_LEN + HEAD_REQUESTS_NUM * SHA_LEN)) {
        error("Unable to init random data");
    }

    connections = malloc(SOCK_NUM * sizeof(connection_data_s));
    if (!connections) {
        error("Unable to allocate memory");
    }
    memset(connections, 0, SOCK_NUM * sizeof(connection_data_s));
    response_watchers = malloc(SOCK_NUM * sizeof(ev_io));
    if (!response_watchers) {
        error("Unable to allocate memory");
    }
    memset(response_watchers, 0, SOCK_NUM * sizeof(ev_io));


    // open all sockets and create watchers for them to read responses
    for(int i = 0 ; i < SOCK_NUM ; i++) {
        connections[i].sd = open_socket();
        if (!connections[i].sd) {
            error("Unable to create connection #%i", i);
        }
        response_watchers[i].data = &connections[i];
        connections[i].ready_to_read_watcher = &response_watchers[i];
        ev_io_init(&response_watchers[i], on_responce, connections[i].sd, EV_READ);
    }

    // init timer data and create timer
    timer_data.head_mess_num = HEAD_REQUESTS_NUM / TIMER_ELAPSES_IN_SECOND;
    timer_data.put_mess_num = PUT_REQUESTS_NUM / TIMER_ELAPSES_IN_SECOND;
    timer_data.head_last_socket = 0;
    timer_data.put_last_socket = 0;
    timer_data.last_time = time(NULL);
    ev_timer_init (&timeout_watcher, on_timeout, 1.0 / TIMER_ELAPSES_IN_SECOND, 1.0 / TIMER_ELAPSES_IN_SECOND);

    // start timer and socket watchers
    for(int i = 0 ; i < SOCK_NUM ; i++) {
        ev_io_start(e_loop, &response_watchers[i]);
    }
    ev_timer_start (e_loop, &timeout_watcher);
    ev_run (e_loop, 0);
    // TODO free

    return 0;
}
//-------------------------------------------------------------------

static void parse_opts(int argc, char **argv) {
    for (int i = 1 ; i < argc ; i++) {
        if (0 == strcmp(argv[i], "--help")) {
            usage();
            exit(0);
        } else if (0 == strcmp(argv[i], "-c")) {
            SOCK_NUM = atoi(argv[++i]);
        } else if (0 == strcmp(argv[i], "-m")) {
            int m = atoi(argv[++i]);
            HEAD_REQUESTS_NUM *= m;
            PUT_REQUESTS_NUM *= m;
        } else if (0 == strcmp(argv[i], "-h")) {
            HOST = argv[++i];
        } else if (0 == strcmp(argv[i], "-p")) {
            PORT = atoi(argv[++i]);
        } else if (0 == strcmp(argv[i], "-s")) {
            TIMER_ELAPSES_IN_SECOND = atoi(argv[++i]);
        } else {
            log_err("Unknown option");
            usage();
            exit(1);
        }
    }
}
//-------------------------------------------------------------------

static void usage() {
    printf("options: \n\r"
            "-c number of connections to use (default %i)\n\r"
            "-m multipler for default number of PUT (%li) and GET (%li) requests\n\r"
            "-h hostname (default %s)\n\r"
            "-p port (default %i)\n\r"
            "-s number of times timer elapses in second (default %li)\n\r",
            SOCK_NUM_DEF,
            (long)HEAD_REQUESTS_NUM_DEF,
            (long)PUT_REQUESTS_NUM_DEF,
            HOST_DEF, PORT_DEF,
            (long)TIMER_ELAPSES_IN_SECOND_DEF);
}
//-------------------------------------------------------------------

static int open_socket() {
    int sockfd, buf_size = 9216, flags;
    struct hostent *he;

    struct sockaddr_in their_addr;
    he = gethostbyname(HOST);
    if(he == NULL) {
        plog_err("in gethostbyname");
        goto error;
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1) {
        plog_err("in socket()");
        goto error;
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &buf_size, sizeof(buf_size)) < 0) {
        plog_err("cannot set socket options\n\r");
        goto error;
    }
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &buf_size, sizeof(buf_size)) < 0) {
        plog_err("cannot set socket options\n\r");
        goto error;
    }

    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(PORT);
    their_addr.sin_addr = *((struct in_addr *)he->h_addr_list[0]);
    memset(&(their_addr.sin_zero), '\0', 8);

    if(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) {
        plog_err("in connect");
        goto error;
    }

    flags = fcntl(sockfd, F_GETFL, 0);
    fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);

    return sockfd;

error:
    if(sockfd) {
        close(sockfd);
    }
    return 0;
}
//---------------------------------------------------------------------

static void on_timeout(struct ev_loop *loop, ev_timer *w, int revents) {
    int curr_time, responses = 0, requests = 0, queued = 0, dequeued = 0;
    head_request_s *new_head_request;
    put_request_s *new_put_request;
    static int elapsed = 0;

    elapsed++;
    curr_time = time(NULL);
    if (curr_time > timer_data.last_time) {
        // count how many responses were received
        for (int i = 0 ; i < SOCK_NUM ; i++) {
            responses += connections[i].resp_recv;
            requests += connections[i].req_sent;
            queued += connections[i].queued;
            dequeued += connections[i].dequeued;
        }
        printf("requests sent: %i, responses: %i, queued: %i, dequeued %i, elapsed %i\n\r",
                requests, responses, queued, dequeued, elapsed);
        elapsed = 0;
        timer_data.last_time = curr_time;
    }

    for (int i = 0 ; i < timer_data.head_mess_num ; i++) {
        new_head_request = head_request();
        do {
            timer_data.head_last_socket++;
            if (SOCK_NUM == timer_data.head_last_socket) {
                timer_data.head_last_socket = 0;
            }
        } while(!connections[timer_data.head_last_socket].sd);
        add_next_response_len(&connections[timer_data.head_last_socket], HEAD_RESPONSE_LEN);
        send_message(&connections[timer_data.head_last_socket], new_head_request, HEAD_REQUEST_LEN);
    }
    for (int i = 0 ; i < timer_data.put_mess_num ; i++) {
        new_put_request = put_request();
        do {
            timer_data.put_last_socket++;
            if (SOCK_NUM == timer_data.put_last_socket) {
                timer_data.put_last_socket = 0;
            }
        } while(!connections[timer_data.put_last_socket].sd);
        add_next_response_len(&connections[timer_data.put_last_socket], PUT_RESPONSE_LEN);
        send_message(&connections[timer_data.put_last_socket], new_put_request, PUT_REQUEST_LEN);
    }

}
//-------------------------------------------------------------------

static void add_next_response_len(connection_data_s *c_data, int len) {
    next_response_s *next_resp = malloc(sizeof(next_response_s));
    memset(next_resp, 0, sizeof(next_response_s));
    next_resp->len = len;

    if(NULL == c_data->res_head) {
        c_data->res_head = c_data->res_tail = next_resp;
    } else {
        c_data->res_tail->next = next_resp;
        c_data->res_tail = next_resp;
    }
}
//-------------------------------------------------------------------

static void get_next_response_len(connection_data_s *c_data) {
    if (c_data->res_head) {
        c_data->next_res_len = c_data->res_head->len;
        next_response_s *to_free = c_data->res_head;
        c_data->res_head = c_data->res_head->next;
        if(0 == c_data->res_head) {
            c_data->res_tail = 0;
        }
        free(to_free);
    } else {
        c_data->next_res_len = 0;
    }
}
//-------------------------------------------------------------------

static void send_message(connection_data_s *c_data, void *message, int len) {
    int nbytes;
    ev_io *ready_to_write_watcher;
    message_s *new_message;

    if(NULL == c_data->req_head) {
        nbytes = write(c_data->sd, message, len);
        if( (nbytes == -1 && EAGAIN == errno) || (nbytes >= 0 && nbytes != len)) {
            new_message = malloc (sizeof(message_s));
            if(!new_message) {
                error("can't allocate memory");
            }
            new_message->message = message;
            new_message->next = NULL;
            new_message->offset = (nbytes == -1) ? 0 : nbytes;
            new_message->len = len;
            c_data->req_head = c_data->req_tail = new_message;
            ready_to_write_watcher = malloc(sizeof(ev_io));
            if(!ready_to_write_watcher) {
                error("can't allocate memory");
            }
            ready_to_write_watcher->data = c_data;
            ev_io_init(ready_to_write_watcher, on_ready_to_write, c_data->sd, EV_WRITE);
            ev_io_start(e_loop, ready_to_write_watcher);
            c_data->ready_to_write_watcher = ready_to_write_watcher;
            c_data->queued++;
        } else if (nbytes == -1) {
            plog_err("cannot write to socket %i", c_data->sd);
            socket_error(c_data);
        } else {
            free(message);
            c_data->req_sent++;
        }
    } else {
        new_message = malloc (sizeof(message_s));
        if(!new_message) {
            error("can't allocate memory");
        }
        new_message->message = message;
        new_message->next = NULL;
        new_message->offset = 0;
        new_message->len = len;
        c_data->req_tail->next = new_message;
        c_data->queued++;
    }
}

//-------------------------------------------------------------------

static void on_ready_to_write(struct ev_loop *loop, ev_io *w, int revents) {
    int nbytes, len;
    connection_data_s *c_data = w->data;
    message_s *message = c_data->req_head, *next_message;

    while (message) {
        len = message->len - message->offset;
        nbytes = write(c_data->sd, message->message + message->offset, len);
        if( (nbytes == -1 && EAGAIN == errno) || (nbytes >= 0 && nbytes != len)) {
            message->offset += (nbytes == -1) ? 0 : nbytes;
            c_data->req_head = message;
            log_debug("sock %i, left %li", c_data->sd, c_data->queued);
            break;
        } else if (nbytes == -1) {
            plog_err("cannot write to socket %i", c_data->sd);
            socket_error(c_data);
            break;
        } else {
            next_message = message->next;
            free(message->message);
            free(message);
            message = next_message;
            c_data->req_sent++;
            c_data->queued--;
            c_data->dequeued++;
        }
    }

    if (NULL == message) {
        c_data->req_head = NULL;
        c_data->ready_to_write_watcher = 0;
        ev_io_stop (EV_A_ w);
        free(w);
    }
}
//-------------------------------------------------------------------

static void *head_request() {
    head_request_s *message = malloc(HEAD_REQUEST_LEN);
    if(! message){
        error("cannot allocate memory");
    }
    memset(message, 0, HEAD_REQUEST_LEN);
    message->command = COMMAND_HEAD;
    if(rd_get(SHA_LEN, (char *)message->key)) {
        error("cannon get data");
    }
    return (void*)message;
}
//-------------------------------------------------------------------

static void *put_request() {
    put_request_s *message = malloc(PUT_REQUEST_LEN);
    if(! message) {
        error("cannon allocate memory");
    }
    memset(message, 0, PUT_REQUEST_LEN);
    message->command = COMMAND_PUT;
    if(rd_get(DATA_LEN, message->data)) {
        error("cannon get data");
    }
    SHA1((unsigned char *)message->data, DATA_LEN, message->key);
    return (void*)message;
}
//-------------------------------------------------------------------

static void on_responce(struct ev_loop *loop, ev_io *w, int revents) {
    long nbytes;
    char buf[RESPONSE_LEN + 1];
    connection_data_s *c_data = (connection_data_s *)w->data;
    memset(buf,0,sizeof(buf));

    nbytes = read(c_data->sd, buf, RESPONSE_LEN);
    if( nbytes == -1 && EAGAIN == errno ) {
        // nothing was read. Wait for another event
        return;
    } else if (nbytes == -1) {
        // error happened. Stop all watchers and close socket
        plog_err("cannot read from socket %i", c_data->sd);
        socket_error(c_data);
    } else if (nbytes == 0) {
        // end of file - socket was closed
        log_info("connection on socket %i was closed by client", c_data->sd);
        socket_error(c_data);
    } else {
        if (0 == c_data->next_res_len) {
            get_next_response_len(c_data);
        }
        c_data->resp_part_rcvd += nbytes;
        while(c_data->resp_part_rcvd >= c_data->next_res_len) {
            if(!c_data->next_res_len) {
                break;
            }
            c_data->resp_recv++;
            c_data->resp_part_rcvd -= c_data->next_res_len;
            get_next_response_len(c_data);
        }
    }
}

