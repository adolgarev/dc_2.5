.PHONY: clean all

COMPONENTS=compute_node_mock compute_node_test load_gen compute_node


all:
	$(foreach component,$(COMPONENTS),cd $(component) && $(MAKE) && cd -;)

clean:
	$(foreach component,$(COMPONENTS),cd $(component) && $(MAKE) clean && cd -;)
	rm -f *~
