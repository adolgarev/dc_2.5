
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include "protocol.h"
#include "log.h"

#define PORT_DEF 4000
#define HOST_DEF "localhost"

static unsigned int PORT = PORT_DEF;
static char *HOST = HOST_DEF;
static char put = 0;
static char head = 0;
static char get = 0;

static void parse_opts(int argc, char **argv);
static void read_data(unsigned char *buf);
static void usage();
static int open_socket();
static void head_request(head_request_s *hr, unsigned char *sha);
static void get_request(get_request_s *gr, unsigned char *sha);
static void put_request(put_request_s *pr, unsigned char *data, unsigned char *sha);
static int check_head_response(head_response_s *hrs);
static int check_put_response(put_response_s *prs);
static int check_get_response(get_response_s *grs, unsigned char *data);
static void send_message(int sd, char *message, long len);
static void receive_message(int sd, char *message, long len);

//-------------------------------------------------------------------

int main(int argc, char **argv) {
    int sd = 0, res = 0;
    unsigned char sha[SHA_DIGEST_LENGTH];
    unsigned char data[DATA_LEN];
    head_request_s hr;
    head_response_s hrs;
    put_request_s pr;
    put_response_s prs;
    get_request_s gr;
    get_response_s grs;

    parse_opts(argc, argv);
    if (0 == head && 0 == put && 0 == get) {
        head = put = get = 1;
    }
    read_data(data);

    SHA1(data, DATA_LEN, sha);

    sd = open_socket();
    if (!sd) {
        log_syserr("Unable to create connection");
    }

    if (1 == head) {
        head_request(&hr, sha);
        send_message(sd, (char *)&hr, HEAD_REQUEST_LEN);
        log_info("head request sent");
        receive_message(sd, (char *)&hrs, HEAD_RESPONSE_LEN);
        log_info("response received");
        if(!check_head_response(&hrs)) {
            log_info("FAIL: bad head response");
            res = 1;
            goto exit;
        }
        log_info("head response is ok");
    }

    if (1 == put) {
        put_request(&pr, data, sha);
        send_message(sd, (char *)&pr, PUT_REQUEST_LEN);
        log_info("put request sent");
        receive_message(sd, (char *)&prs, PUT_RESPONSE_LEN);
        log_info("response received");
        if(!check_put_response(&prs)) {
            log_info("FAIL: bad put response");
            res = 1;
            goto exit;
        }
        log_info("put response is ok");
    }

    if (1 == get) {
        get_request(&gr, sha);
        send_message(sd, (char *)&gr, GET_REQUEST_LEN);
        log_info("get request sent");
        receive_message(sd, (char *)&grs, GET_RESPONSE_LEN);
        log_info("response received");
        if(!check_get_response(&grs, data)) {
            log_info("FAIL: bad get response");
            res = 1;
            goto exit;
        }
        log_info("get response is ok");
    }

exit:
    if (sd) {
        close(sd);
    }
    return res;

}
//-------------------------------------------------------------------

static void parse_opts(int argc, char **argv) {
    for (int i = 1 ; i < argc ; i++) {
        if (0 == strcmp(argv[i], "--help")) {
            usage();
            exit(0);
        } else if (0 == strcmp(argv[i], "-h")) {
            HOST = argv[++i];
        } else if (0 == strcmp(argv[i], "-p")) {
            PORT = atoi(argv[++i]);
        } else if (0 == strcmp(argv[i], "-a")) {
            i++;
            if (0 == strcmp(argv[i], "head")) head = 1;
            else if (0 == strcmp(argv[i], "put")) put = 1;
            else if (0 == strcmp(argv[i], "get")) get = 1;
            else log_syserr("unknown action");
        } else {
            log_syserr("unknown option");
        }
    }
}
//-------------------------------------------------------------------

static void read_data(unsigned char *buf) {
    long n;
    n = read(0, buf, DATA_LEN);
    if (n <= 0) {
        plog_syserr("cannot read data");
    }
}
//-------------------------------------------------------------------

static void usage() {
    printf("usage: cn_test [options]\n\r"
            "gets data from stdin"
            "options:\n\r"
            "-h hostname\n\r"
            "-p port\n\r"
            "-a action (put, get or head)");
}
//-------------------------------------------------------------------

static int open_socket() {
    int sockfd, buf_size = 9216;
    struct hostent *he;

    struct sockaddr_in their_addr;
    he = gethostbyname(HOST);
    if(he == NULL) {
        plog_err("in gethostbyname");
        goto error;
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1) {
        plog_err("in socket()");
        goto error;
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &buf_size, sizeof(buf_size)) < 0) {
        plog_err("cannot set socket options\n\r");
        goto error;
    }
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &buf_size, sizeof(buf_size)) < 0) {
        plog_err("cannot set socket options\n\r");
        goto error;
    }

    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(PORT);
    their_addr.sin_addr = *((struct in_addr *)he->h_addr_list[0]);
    memset(&(their_addr.sin_zero), '\0', 8);

    if(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) {
        plog_err("in connect");
        goto error;
    }

    return sockfd;

error:
    if(sockfd) {
        close(sockfd);
    }
    return 0;
}
//---------------------------------------------------------------------

static void head_request(head_request_s *message, unsigned char *sha) {
    memset(message, 0, HEAD_REQUEST_LEN);
    message->command = COMMAND_HEAD;
    memcpy(message->key, sha, SHA_LEN);
}
//-------------------------------------------------------------------

static void get_request(get_request_s *message, unsigned char *sha) {
    memset(message, 0, GET_REQUEST_LEN);
    message->command = COMMAND_GET;
    memcpy(message->key, sha, SHA_LEN);
}
//-------------------------------------------------------------------

static void put_request(put_request_s *message, unsigned char *data, unsigned char *sha) {
    memset(message, 0, PUT_REQUEST_LEN);
    message->command = COMMAND_PUT;
    memcpy(message->key, sha, SHA_LEN);
    memcpy(message->data, data, DATA_LEN);
}
//-------------------------------------------------------------------

static int check_head_response(head_response_s *hrs) {
    if(hrs->code != CODE_OK && hrs->code != CODE_NOT_FOUND) {
        return 0;
    }
    return 1;
}
//-------------------------------------------------------------------

static int check_put_response(put_response_s *prs) {
    if(prs->code != CODE_OK) {
        return 0;
    }
    return 1;
}
//-------------------------------------------------------------------

static int check_get_response(get_response_s *grs, unsigned char *data) {
    if(grs->code != CODE_OK && grs->code != CODE_NOT_FOUND) {
        return 0;
    }
    if(memcmp(grs->data, data, DATA_LEN)) {
        return 0;
    }
    return 1;
}
//-------------------------------------------------------------------

static void send_message(int sd, char *message, long len) {
    long nbytes;

    nbytes = write(sd, message, len);
    if (nbytes <= 0) {
        // error happened. Close socket
        close(sd);
        plog_syserr("cannot read from socket");
    }
}
//-------------------------------------------------------------------

static void receive_message(int sd, char *message, long len) {
    long nbytes;

    memset(message, 0, sizeof(message));

    nbytes = read(sd, message, len);
    if (nbytes == -1) {
        // error happened. Close socket
        close(sd);
        plog_syserr("cannot read from socket");
    } else if (nbytes == 0) {
        // end of file - socket was closed
        close(sd);
        log_syserr("connection was closed by server");
    }
}
