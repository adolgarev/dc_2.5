#ifndef LOG_H
#define LOG_H

#include <errno.h>
#include <string.h>

// deprecated
#define plog_syserr(fmt, ...)       \
    {fprintf(stderr, "%s:%d ERROR: " fmt " [%m]\n", \
    __FILE__, __LINE__, ##__VA_ARGS__); \
    fprintf(stderr, "errno message: %s\r\n", strerror( errno ) ); \
    exit(1);}

// deprecated
#define plog_err(fmt, ...)       \
    {fprintf(stderr, "%s:%d ERROR: " fmt "\n", \
    __FILE__, __LINE__, ##__VA_ARGS__); \
    fprintf(stderr, "errno message: %s\r\n", strerror( errno ) );}

#define log_err(fmt, ...)       \
    fprintf(stderr, "%s(%s):%d ERROR: " fmt "\n", \
    __FILE__, __func__, __LINE__, ##__VA_ARGS__)

#define log_syserr(fmt, ...)    \
    fprintf(stderr, "%s(%s):%d ERROR: " fmt "(%m)\n", \
    __FILE__, __func__, __LINE__, ##__VA_ARGS__)

#define log_info(fmt, ...)       \
    fprintf(stdout, "%s(%s):%d INFO: " fmt "\n", \
    __FILE__, __func__, __LINE__, ##__VA_ARGS__)

#define log_debug(fmt, ...)     \
    fprintf(stdout, "%s(%s):%d DEBUG: " fmt "\n", \
    __FILE__, __func__, __LINE__, ##__VA_ARGS__)

#endif //LOG_H
