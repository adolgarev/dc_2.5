#ifndef PROTOCOL_H
#define PROTOCOL_H

typedef unsigned char version_t;
typedef unsigned char code_t;
typedef unsigned char command_t;

typedef enum {
    COMMAND_HEAD,
    COMMAND_PUT,
    COMMAND_GET
} command_e;

typedef enum {
    CODE_OK,
    CODE_NOT_FOUND,
    CODE_ERROR
} code_e;

#define DATA_LEN 16384
#define SHA_LEN 20
#define GEN_NUM_LEN 8

#define REQUEST_COMMON  version_t version; \
                        command_t command; \
                        unsigned char key[SHA_LEN];

#define RESPONSE_COMMON version_t version; \
                        code_t code;

typedef struct _request_s {
    #pragma pack(1)
    REQUEST_COMMON
} request_s;

typedef request_s head_request_s;
typedef request_s get_request_s;

typedef struct _put_request_s {
    #pragma pack(1)
    REQUEST_COMMON
    char gen_number[GEN_NUM_LEN];
    char data[DATA_LEN];
} put_request_s;

typedef struct _response_s {
    #pragma pack(1)
    RESPONSE_COMMON
} response_s;

typedef response_s put_response_s;

typedef struct _head_response_s {
    #pragma pack(1)
    RESPONSE_COMMON
    char gen_number[GEN_NUM_LEN];
} head_response_s;

typedef struct _get_response_s {
    #pragma pack(1)
    RESPONSE_COMMON
    char gen_number[GEN_NUM_LEN];
    char data[DATA_LEN];
} get_response_s;

#define RESPONSE_LEN sizeof(response_s)
#define REQUEST_LEN sizeof(request_s)
#define PUT_REQUEST_LEN sizeof(put_request_s)
#define HEAD_REQUEST_LEN sizeof(head_request_s)
#define GET_REQUEST_LEN sizeof(get_request_s)
#define PUT_RESPONSE_LEN sizeof(put_response_s)
#define HEAD_RESPONSE_LEN sizeof(head_response_s)
#define GET_RESPONSE_LEN sizeof(get_response_s)

#endif //PROTOCOL_H
